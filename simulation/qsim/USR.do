onerror {quit -f}
vlib work
vlog -work work USR.vo
vlog -work work USR.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.USR_vlg_vec_tst
vcd file -direction USR.msim.vcd
vcd add -internal USR_vlg_vec_tst/*
vcd add -internal USR_vlg_vec_tst/i1/*
add wave /*
run -all
