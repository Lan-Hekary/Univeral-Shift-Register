library verilog;
use verilog.vl_types.all;
entity USR is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        load            : in     vl_logic;
        enable          : in     vl_logic;
        dir             : in     vl_logic;
        rot             : in     vl_logic;
        data_in         : in     vl_logic_vector(7 downto 0);
        data_out        : out    vl_logic_vector(7 downto 0)
    );
end USR;
