module USR(
	input clk,
	input reset,		// reset
	input load,			// load the data from the input port
	input enable,		// enable for the clock
	input dir,			// 0 for left , 1 for right
	input rot,			// 0 no rotation , 1 rotation in the dir
	input [7:0] data_in,
	output reg [7:0] data_out
	);

always @(posedge clk, negedge reset)begin
	if(!reset)
		data_out <=8'b0;
	else if(enable)begin
		if (load) begin
			data_out<=data_in;
		end else
			case({dir,rot})
			2'b00: data_out<={data_out[6:0],1'b0};
			2'b10: data_out<={1'b0,data_out[7:1]};
			2'b01: data_out<={data_out[6:0],data_out[7]};
			2'b11: data_out<={data_out[0],data_out[7:1]};
			endcase	
	end
end
endmodule
